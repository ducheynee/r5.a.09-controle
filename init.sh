#!/bin/bash
docker network create -d bridge external
cd ./Treafik && docker compose up -d --build
cd ../Logging && docker compose up -d --build
cd ../Joomla && docker compose up -d --build
cd ../Keycloak && docker compose up -d --build
cd ../Ghost && docker compose up -d --build
